terraform {
  backend "http" {
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

provider "aws" {
  region = "eu-west-1"
}

# Create VPC using module
module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "2.77.0"

  name = "ec2_vpc"
  cidr = "10.0.0.0/25"

  azs                  = ["eu-west-1a", "eu-west-1b"]
  public_subnets       = ["10.0.0.0/26", "10.0.0.64/26"]
  enable_dns_hostnames = true
  enable_dns_support   = true
}

# Get image from data source
data "aws_ami" "amazon-linux" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-kernel*-x86_64-gp2"]
  }
}

# Create template for EC2 images
resource "aws_launch_configuration" "ec2" {
  name_prefix     = "launch-"
  image_id        = data.aws_ami.amazon-linux.id
  instance_type   = "t2.micro"
  user_data       = file("user-data.sh")
  security_groups = [aws_security_group.ec2_instance.id]

  lifecycle {
    create_before_destroy = true
  }
}

# Create autoscaling group
resource "aws_autoscaling_group" "ec2" {
  name_prefix          = "asg-"
  min_size             = 0
  max_size             = 2
  desired_capacity     = 2
  launch_configuration = aws_launch_configuration.ec2.name
  vpc_zone_identifier  = module.vpc.public_subnets

  lifecycle {
    create_before_destroy = true
  }

  tag {
    key                 = "Name"
    value               = "EC2 autoscaling"
    propagate_at_launch = true
  }
}

# Create Network Load Balancer
resource "aws_lb" "ec2" {
  name_prefix        = "nlb-"
  internal           = false
  load_balancer_type = "network"
  subnets            = module.vpc.public_subnets
}

# Create Load Balancer Listener
resource "aws_lb_listener" "ec2" {
  load_balancer_arn = aws_lb.ec2.arn
  port              = "80"
  protocol          = "TCP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.ec2.arn
  }
}

# Create Target Group
resource "aws_lb_target_group" "ec2" {
  name_prefix = "tg-"
  port        = 31555
  protocol    = "TCP"
  vpc_id      = module.vpc.vpc_id
}

# Attach EC2 instances in Target Group
resource "aws_autoscaling_attachment" "ec2" {
  lb_target_group_arn    = aws_lb_target_group.ec2.arn
  autoscaling_group_name = aws_autoscaling_group.ec2.id
}

# Create schedule for scaling
resource "aws_autoscaling_schedule" "schedule" {
  for_each               = var.schedule
  scheduled_action_name  = each.value.scheduled_action_name
  min_size               = each.value.min_size
  max_size               = each.value.max_size
  desired_capacity       = each.value.desired_capacity
  recurrence             = each.value.recurrence
  time_zone              = each.value.time_zone
  start_time             = each.value.start_time
  autoscaling_group_name = aws_autoscaling_group.ec2.name
}

# Create security group for EC2 instance
resource "aws_security_group" "ec2_instance" {
  name_prefix = "secg-"

  ingress {
    from_port   = 31555
    to_port     = 31555
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = module.vpc.vpc_id
}
