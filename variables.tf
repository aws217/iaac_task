variable "schedule" {
  description = "Scaling schedule"
  type        = map(any)

  default = {
    scale_down = {
      scheduled_action_name = "scale_down",
      min_size              = 0,
      max_size              = 2,
      desired_capacity      = 1,
      recurrence            = "0 18 * * *",
      time_zone             = "CET",
      start_time            = "2022-08-26T18:00:00Z"
    },
    scale_up = {
      scheduled_action_name = "scale_up",
      min_size              = 0,
      max_size              = 2,
      desired_capacity      = 2,
      recurrence            = "0 08 * * *",
      time_zone             = "CET",
      start_time            = "2022-08-26T08:00:00Z"
    }
  }
}
