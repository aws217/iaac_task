# Iaac Task



## Option 4

The Task - Infrastructure as a code

## Deployment instructions

```
Prerequisites:
brew install awscli
brew tap hashicorp/tap
brew install hashicorp/tap/terraform
```

```
Deployment:
aws configure   <- Authenticate with IAM Access Key ID and Secret Access Key in AWS cli
vim main.tf    <- Configure backend local or http to store .tfstate file(I am using Gitlab as backend, see [HERE](https://docs.gitlab.cn/14.0/ee/user/infrastructure/terraform_state.html) )
terraform init  <- Initialize and download providers and modules
terraform plan  <- See what is about to pe provisioned
terraform apply <- Deploy resources
```

## Info
I have used the `terraform-aws-modules/vpc/aws` module in order to setup the VPC faster and easier

## Bonus

1. httpd is installed with userdata
2. When hitting the NLB url, it connects to EC2 on port 31555 and it displays the default home page
